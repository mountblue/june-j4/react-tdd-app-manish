import React from "react";
import ReactDOM from "react-dom";
import { mount, shallow } from "enzyme";
import { configure } from "enzyme";
configure({ adapter: new Adapter() });
import Adapter from "enzyme-adapter-react-16";
import TodoList from "./components/TodoList";
import { spy } from "sinon";
import DeleteButton from "./components/TodoDelete";
import CheckBox from "./components/TodoCheckBox";

describe("<TodoList />", () => {
  it("should render a empty list of todo tasks", function() {
    const MockTests = [];
    var wrapper = shallow(<TodoList todoList={MockTests} />);
    expect(wrapper.find("li").length).toEqual(MockTests.length);
  });

  it("should render a list of todo tasks", function() {
    const MockTests = [
      {
        id: 1,
        text: "create todo app",
        status: false
      }
    ];
    var wrapper = shallow(<TodoList todoList={MockTests} />);
    expect(wrapper.find("li").length).toEqual(MockTests.length);
  });

  it("should call deleteItem when DELETE is clicked", () => {
    const deleteItemSpy = spy();
    const wrapper = mount(<DeleteButton deleteItem={deleteItemSpy} />);
    wrapper.find("button").simulate("click");
    expect(deleteItemSpy.calledOnce).toEqual(true);
  });

  it("should call checkedItem when CHECKBOX is clicked", () => {
    const checkboxItemSpy = spy();
    const wrapper = mount(<CheckBox checkedItem={checkboxItemSpy} />);
    wrapper.find("input").simulate("click");
    expect(checkboxItemSpy.calledOnce).toEqual(true);
  });

});
