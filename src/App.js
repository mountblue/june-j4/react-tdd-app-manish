import React, { Component } from "react";
import "./App.css";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";
import uuid from "uuid";

class App extends Component {
  constructor() {
    super();
    this.state = {
      todoList: []
    };
  }

  getTodoList(){
   return this.state.todoList;
  }

  checkedItem = id =>{
    const todoTasks = this.getTodoList();
    const filterTask = todoTasks.map(task => {
      if(task.id === id)
          task.status=!task.status;
          return task;
    });
    this.setState({ todoList: filterTask });
  }

  deleteItem = id =>{
   let todoList = this.getTodoList();
   todoList = todoList.filter(item =>{
        return item.id != id ? item : false
   })
   this.setState({todoList : [].concat(todoList)}); 
  }

  addItem = task => {
    if(task==="")
    return;
    let itemObj = {
      id: uuid.v4(),
      text: task,
      status: false
    };
    this.setState({todoList : [].concat(this.getTodoList()).concat([itemObj])});
  };

  render() {
    return (
      <div className="App">
        <h1 className="todo-header">TODO APPLICATION</h1>
        <TodoInput onSubmit={this.addItem}/>
        <TodoList todoList={this.state.todoList}  deleteItem={this.deleteItem} checkedItem={this.checkedItem}/>
      </div>
    );
  }
}

export default App;
