import React from "react";
import { mount, shallow } from "enzyme";
import { configure } from "enzyme";
configure({ adapter: new Adapter() });
import Adapter from "enzyme-adapter-react-16";
import TodoInput from "./components/TodoInput";
import App from "./App";

describe("<TodoInput />", () => { 
  it("should have an input field to add the todo task", function() {
    var wrapper = shallow(<TodoInput />);
    expect(wrapper.find("input").length).toEqual(1);
  });

  it('onSubmit attribute should be of type function`', () => {
    var wrapper = shallow(<TodoInput />);
    expect(
      typeof wrapper.props().onSubmit === 'function'
    ).toBe(true);
  });

  it('`<input>` element should be of type `text', () => {
    var wrapper = shallow(<TodoInput />);
    expect(
      wrapper.find('form').childAt(0).props().type
    ).toBe('text');
  });
  
  it('initially input field should be empty',function(){
    var wrapper = shallow(<TodoInput />);
    let input = wrapper.find('input');
    expect(input.text()).toEqual("");
  })
});
