import React from "react";
import PropTypes from "prop-types";
import DeleteButton from "./TodoDelete";
import CheckBox from "./TodoCheckBox";

export default function TodoList(props) {
  return (
    <ul>
      {props.todoList.map(item => {
        return (
          <li key={item.id} className="todo-container">
            <CheckBox checkedItem={props.checkedItem} id={item.id} checked={item.status}/>
            <span id={item.id} className={item.status ? "checked" : "unchecked"} status={item.status.toString()}>
              {item.text}
            </span>
            <DeleteButton deleteItem={props.deleteItem} id={item.id} />
          </li>
        );
      })}
    </ul>
  );

  TodoList.propTypes = {
    todoList: PropTypes.array.isRequired
  };
}
