import React, { Component } from "react";
import PropTypes from "prop-types";

class TodoInput extends Component {
  onSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(event.target[0].value);
    event.target[0].value ="";
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <input type="text" placeholder="What's on your mind"/>
      </form>
    );
  }
}

TodoInput.propTypes = {
  onSubmit: PropTypes.func
};

export default TodoInput;
