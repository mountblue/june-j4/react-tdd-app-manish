import React from 'react';

export default function DeleteButton(props) {
    let deleteItem  = (event) =>{
    event.preventDefault();
    props.deleteItem(props.id);
}
    return <button onClick={deleteItem}>DELETE</button>
}