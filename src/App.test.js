import React from "react";
import ReactDOM from "react-dom";
import { mount, shallow } from "enzyme";
import { configure } from 'enzyme';
configure({ adapter: new Adapter() });
import Adapter from 'enzyme-adapter-react-16';
import App from "./App";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";

describe("<App />", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render TodoInput and TodoList', () => {
    const wrapper = shallow(<App/>);
    expect(wrapper.containsAllMatchingElements([
      <TodoInput/>,
      <TodoList/>
    ])).toEqual(true);
  });

  it("should have an input component to take the input", function() {
    var wrapper = shallow(<App />);
    expect(wrapper.find('TodoInput').length).toEqual(1);
  });

  it("should have a state with an array", function() {
    var wrapper = shallow(<App />);
    expect(wrapper.state('todoList')).toEqual([]);
  });

  it("it should add an item to the array", function() {
    var wrapper = shallow(<App />);
    wrapper.instance().addItem("complete todo");
    expect(wrapper.state('todoList')[0].text).toEqual("complete todo");
  });

  it('should have an on submit function',function(){
    var wrapper = shallow(<App />);
    let TodoInput = wrapper.find('TodoInput');
    let addItem = wrapper.instance().addItem;
    expect(TodoInput.prop('onSubmit')).toEqual(addItem);
  })

  it("should call checkedItem when checkbox is clicked", () =>{
    var wrapper = shallow(<App />);
    wrapper.instance().addItem("complete todo");
    const id=wrapper.state('todoList')[0].id;
    wrapper.instance().checkedItem(id);
    expect(wrapper.state('todoList')[0].status).toEqual(true);
  })

  it("should call deleteItem when delete is pressed", () =>{
    var wrapper = shallow(<App />);
    wrapper.instance().addItem("complete todo");
    const id=wrapper.state('todoList')[0].id;
    wrapper.instance().deleteItem(id);
    expect(wrapper.state('todoList')).toEqual([]);
  })
});
